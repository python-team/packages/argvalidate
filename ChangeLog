2009-03-24  Stephan Peijnik  <stephan@peijnik.at>

	* argvalidate.py:
	  + Bump version number to 0.9.0 and release.

2009-03-24  Stephan Peijnik  <stephan@peijnik.at>

	* runtests.py:
	  + Removed support for sptest.
        * argvalidate.py:
          + (Nearly) complete re-write of checking code, using inspect module.
          + Dropped call argument length checks, these are done by Python
            too.
          + Introduced new accepts and returns decorators.
          + Marked old decorators (method_args, func_args, return_value) as
            deprecated.
          + Bumped version to 0.9.0-rc0.
        * argvalidate_tests.py:
          + Removed argument length tests, checks removed.
          + Replaced old method_args, func_args and return_value decorators
            with new accepts (both for methods and functions) and returns
            decorators.

2009-03-05  Stephan Peijnik  <stephan@peijnik.at>

	* setup.py:
	  + Updated license field to reflect license change.

2009-02-26  Stephan Peijnik  <stephan@peijnik.at>

	* Changed wording of error message in DecoratorLengthException and
	ArgumentLengthException.
	* Added DecoratorStackingException.
	* Added stacking-sanity checking code to __check_args and result_value.
	* Renamed internal OneOfTuple class to __OneOfTyple.
	* Added unit tests for stacking sanity checks.
	* Updated documentation accordingly.
	* Minor spelling fix in ArgvalidateErrorException's docstring.
	* New version: 0.8.2.

2009-02-25  Stephan Peijnik  <stephan@peijnik.at>

	* Simplified unit tests.
	* Renamed func_check_args and method_check_args to func_args and 
	method_args.
	* Added return_value decorator.
	* Subclassed tuple as OneOfTuple with a custom __repr__ method.
	* Added option of generating warnings instead of exceptions.
	* Added ARGVALIDATE_WARN environment variable handling.
	* Added option of warning for keywword arguments pased as non-keyword
	arguments.
	* Added ARGVALIDATE_WARN_KWARG_AS_ARG variable handling.
	* Added testcases for return_value decorator.
	* Relicensed python-argvalidate under the LGPL3+ TODO.
	* Minor formatting fix (double-space fixed) in 
	DecoratorNonKeyLengthException.
	* Added basic documentation.
	* Made exceptions provide execption-specific attributes such
	as func_name.
	* Removed DecoratorKeyLengthException.
	* Added unit tests for Decorator*Exception.
	* Updated setup.py's trove classifies to reflect license change.
	* New version: 0.8.1.

2009-02-24  Stephan Peijnik  <stephan@peijnik.at>

	* First public release: python-argvalidate 0.8.0.
